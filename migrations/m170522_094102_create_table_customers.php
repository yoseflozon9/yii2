<?php

use yii\db\Migration;

class m170522_094102_create_table_customers extends Migration
{
        public function up()
    {
        $this->createTable('customers', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'mail' => $this->string(), 
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('customers');
    }
}
