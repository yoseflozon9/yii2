<?php

use yii\db\Migration;

/**
 * Handles dropping description from table `customers`.
 */
class m170522_185502_drop_description_column_from_customers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('customers', 'description');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('customers', 'description', $this->text());
    }
}
